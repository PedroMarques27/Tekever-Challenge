# Tekever Challenge  
## Getting started  
Swagger documentation of the endpoints is documented at the endpoint <code>/docs</code>  
These files are also accompanied by a insomnia.json file which allows you to test the endoints quickly.    
Before starting the api, run <code>make start</code> within the showapi folder, this will initialize the docker mysql database running at 3306  

# How it works  
The 'background worker' is composed of 2 threads and a monitor. The Producer thread retrieves the TvShows from the external API and adds them to a queue in the monitor. The consumer thread (which was waiting for new shows) retrieves them and saves them in the mySQL database.    
A user can register by simply providing email and password, user management is available through the enpoints. The login endpoint generates a JWT token which identifies the user and can be utilized for authentication and authorization. This token is automatically saved in the cookies to allow for sort of natural navigation and experience. 
Pagination, sorting and genre filtering were all added to the GET /shows endpoint    
The token is needed in order to add, remove and retrieve favorite shows.  


# What wasn't done  
- The background worker was supposed to have an interface, both threads were supposed to be able to be controlled by providing a simple interface developed with swing to halt or completely stop the process  
- Was only able to develop integration tests for the UserController due to a lack of time  
- Have no idea where to obtain the featured actors  


Pedro Marques  
Software Developer  