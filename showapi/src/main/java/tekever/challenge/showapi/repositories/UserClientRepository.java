package tekever.challenge.showapi.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import tekever.challenge.showapi.entities.UserClient;

public interface UserClientRepository extends JpaRepository<UserClient, String> {

}
