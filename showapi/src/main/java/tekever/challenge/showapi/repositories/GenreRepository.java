package tekever.challenge.showapi.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import tekever.challenge.showapi.entities.Genre;

public interface GenreRepository extends JpaRepository<Genre, String>  {
    
}


