package tekever.challenge.showapi.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import tekever.challenge.showapi.entities.Genre;
import tekever.challenge.showapi.entities.TvShow;

public interface TvShowsRepository extends PagingAndSortingRepository<TvShow, Integer> {
    @Query(value = "SELECT * FROM tv_show WHERE name = ?1 LIMIT 1")
    TvShow findByName(String name);



    @Query(value="SELECT * FROM tv_show tv WHERE tv.id in (SELECT tv_show_id from tv_show_genre WHERE genre_id=?1) ORDER BY ?2")
    List<TvShow> findByGenres(Genre genre, Sort sort);
    
    @Query(value="SELECT * FROM tv_show ORDER BY id")
    List<TvShow> findAllSortBy();

}

