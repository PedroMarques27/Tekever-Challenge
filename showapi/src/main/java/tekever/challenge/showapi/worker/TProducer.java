package tekever.challenge.showapi.worker;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import tekever.challenge.showapi.entities.Episode;
import tekever.challenge.showapi.entities.Genre;
import tekever.challenge.showapi.entities.TvShow;

public class TProducer extends Thread {

    private final IProducer iproducer;
    private boolean stopFlag;

    private int currentId;

    public TProducer(IProducer monitor) {
        this.iproducer = monitor;
        this.stopFlag = false;
        this.currentId = 1;
    }

 

    @Override
    public void run() {
        try {

            while (!this.stopFlag){
               
               

                RequestSpecification request = RestAssured.given();
                Response response = request.get(String.format("https://www.episodate.com/api/show-details?q=%d", currentId));
                try{
                    JSONObject shows = (new JSONObject(response.body().asString())).getJSONObject("tvShow");
                    JSONArray genreArray = shows.getJSONArray("genres");
                    JSONArray episodeArray = shows.getJSONArray("episodes");
                    shows.remove("genres");
                    shows.remove("episodes");

                    Set<Genre> genreList = new HashSet<>();
                    for (Object genre: genreArray){
                        genreList.add(new Genre(genre.toString()));
                    }
                    Set<Episode> episodes = new HashSet<>();
                    for (int i=0; i<episodeArray.length();i++){
                        episodes.add((new Gson()).fromJson(episodeArray.getJSONObject(i).toString(), Episode.class));
                    }
                    TvShow newShow = (new Gson()).fromJson(shows.toString(), TvShow.class);
                    newShow.setGenres(genreList);
                    newShow.setEpisodes(episodes);
                    this.iproducer.publishTvShow(newShow);
                    
                }catch (JSONException e){
                    this.stopFlag = true;
                }
                
               
                currentId++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}