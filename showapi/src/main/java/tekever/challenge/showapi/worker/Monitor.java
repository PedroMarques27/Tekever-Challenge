package tekever.challenge.showapi.worker;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import tekever.challenge.showapi.entities.TvShow;

public class Monitor implements IProducer, IConsumer{

    
    /**
     * reentrant mutual exclusion lock
     */
    private final ReentrantLock rl;
    /**
     * Condition which indicates when there is a new show in the list
     */
    private final Condition newShow;

    private ArrayList<TvShow> shows;
    public Monitor(){
        this.rl = new ReentrantLock();
        this.shows = new ArrayList<>();

        this.newShow = rl.newCondition();


    }
    @Override
    public TvShow waitForNewShow() {
        TvShow newTvShow = null;
        try {
            this.rl.lock();
            while(this.shows.isEmpty()) {
                try {
                    this.newShow.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            

            newTvShow = this.shows.get(0);
            this.shows.remove(0);
        }
        finally {
            rl.unlock();
        }
        return newTvShow;
    }

    @Override
    public void publishTvShow(TvShow newshow) {
         try {
            this.rl.lock();
            this.shows.add(newshow);
            this.newShow.signalAll();
        }
        finally {
            rl.unlock();
        }
    }
}
