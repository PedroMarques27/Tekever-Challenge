package tekever.challenge.showapi.worker;

import tekever.challenge.showapi.entities.TvShow;

public interface IConsumer {
    public TvShow waitForNewShow();

}
