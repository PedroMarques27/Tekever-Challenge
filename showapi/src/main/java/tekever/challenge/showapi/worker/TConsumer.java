package tekever.challenge.showapi.worker;

import tekever.challenge.showapi.entities.Genre;
import tekever.challenge.showapi.entities.TvShow;
import tekever.challenge.showapi.repositories.GenreRepository;
import tekever.challenge.showapi.repositories.TvShowsRepository;

public class TConsumer extends Thread {
    private TvShowsRepository repository;
    private GenreRepository genreRepository;

    private final IConsumer iconsumer;




    public TConsumer(IConsumer _iconsumer, TvShowsRepository repo, GenreRepository genreRepository) {
        this.iconsumer = _iconsumer;
        this.repository = repo;
        this.genreRepository = genreRepository;
    }
   


    @Override
    public void run() {
        
            while (true){
                try {

                    TvShow newShow = (iconsumer.waitForNewShow());
                    for (Genre s: newShow.getGenres()){
                        if (!genreRepository.existsById(s.getGenre()))
                            genreRepository.save(s);
                        
                    }
                repository.save(newShow);
                } catch (Exception e) {
                    ;
                }
            }

       

    }

}