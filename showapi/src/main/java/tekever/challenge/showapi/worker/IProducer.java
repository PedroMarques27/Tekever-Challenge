package tekever.challenge.showapi.worker;

import tekever.challenge.showapi.entities.TvShow;

public interface IProducer {
    public void publishTvShow(TvShow newshow);
}
