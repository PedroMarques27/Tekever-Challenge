package tekever.challenge.showapi.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Episode {
    @Id
    @GeneratedValue
    private int id; 

    @Column
    private int episode;

    @Column
    private int season;
    
    @Column
    private String name;

    @Column
    private String air_date;


    public Episode(){};
   
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getEpisode() {
        return episode;
    }

    public void setEpisode(int episode) {
        this.episode = episode;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
   
    public String getAirDate() {
        return air_date;
    }
    public void setAirDate(String airDate) {
        this.air_date = airDate;
    }
}
