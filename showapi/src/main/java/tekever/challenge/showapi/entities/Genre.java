package tekever.challenge.showapi.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Genre {
    public Genre(){}

    public Genre(String string) {
        this.id = string;
    }

    
    @Id
    private String id;

    
    @ManyToMany(
        cascade = CascadeType.ALL)
    private Set<TvShow> shows = new HashSet<>();

   
    public Set<TvShow> getShows() {
        return shows;
    }

    public void setShows(Set<TvShow> shows) {
        this.shows = shows;
    }

  
    public String getGenre() {
        return id;
    }
    public void setGenre(String genre) {
        this.id = genre;
    }  
    
    @Override
    public String toString(){
        return String.format("Genre %s ", id);
    }
}
