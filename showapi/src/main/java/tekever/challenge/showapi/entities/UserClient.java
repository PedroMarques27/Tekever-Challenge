package tekever.challenge.showapi.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table
public class UserClient {
    @Id 
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String username;

    @OneToMany(cascade = CascadeType.DETACH,orphanRemoval = true, fetch=FetchType.EAGER)
    private Set<TvShow> favorites = new HashSet<>();

    public UserClient(){}
    public UserClient(String _email, String _password){
        this.email = _email;
        this.password = _password;
       
    }
    public UserClient(String _email, String _username, String _password){
        
        this.email = _email;
        this.password = _password;
        this.username = _username;

       
    }


    public String getEmail(){
        return this.email;
    }

    public String getPassword(){
        return this.password;
    }
    public String getUsername(){
        return this.username;
    }
    public void setUsername(String _username){
        this.username = _username;
    }

    public boolean verifyPassword(String _password){
        return String.format("%d", _password.hashCode()).equals(this.password);
    }
    public void hashPassword(){
        this.password = String.format("%d", password.hashCode());
    }

    public void cleanId(){
        this.email = email.strip().toLowerCase();
    }
   
    
    @Override
    public String toString(){
        return String.format("User [%s, %s,  %s]", this.email, this.username, this.password);
    }
    public void addFavorite(TvShow show) {
        this.favorites.add(show);
    }
    public Set<TvShow> getFavorites() {
        return this.favorites;
    }
    public void removeFavorite(TvShow show) {
        this.favorites.removeIf(tvshow -> tvshow == show);
    }
    public void setFavorites(Set<TvShow> favorites2) {
        this.favorites = favorites2;
    }
    public void clearFavorites() {
        this.favorites.clear();
    }
    
    
}
