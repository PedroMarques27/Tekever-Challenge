package tekever.challenge.showapi.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(indexes = @Index(columnList = "name"))
public class TvShow {
    @Id
    @Column
    private int id;

    @Column
    private String name;
    @Column
    private String start_date;
    @Column
    private String end_date;
    @Column
    private String country;
    @Column
    private String network;
    @Column
    private String status;
    @Column
    private String image_thumbnail_path;


    @Column
    private double rating;

    
   
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Episode> episodes = new HashSet<>();

  

    @ManyToMany(
        fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    private Set<Genre> genres = new HashSet<>();


    public TvShow(int id, String name, String start_date, String end_date, String country, String network,
            String status, String image_thumbnail_path){
        this.id = id;
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
        this.country = country;
        this.network = network;
        this.status = status;
        this.image_thumbnail_path = image_thumbnail_path;
    }

    public TvShow(){};
 
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStartDate() {
        return start_date;
    }
    public void setStartDate(String start_date) {
        this.start_date = start_date;
    }
    public String getEndDate() {
        return end_date;
    }
    public void setEndDate(String end_date) {
        this.end_date = end_date;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getNetwork() {
        return network;
    }
    public void setNetwork(String network) {
        this.network = network;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getImagePath() {
        return image_thumbnail_path;
    }
    public void setImagePath(String image_path) {
        this.image_thumbnail_path = image_path;
    }

    public Set<Genre> getGenres() {
        return genres;
    }
    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }
    public Set<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Set<Episode> episodes) {
        this.episodes = episodes;
    }
   
    @Override
    public String toString(){
        return String.format("TvShow %s",name);
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

}
