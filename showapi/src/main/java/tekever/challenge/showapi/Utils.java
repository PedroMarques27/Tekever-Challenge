package tekever.challenge.showapi;

import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import tekever.challenge.showapi.entities.TvShow;
import tekever.challenge.showapi.entities.UserClient;
import tekever.challenge.showapi.repositories.TvShowsRepository;
import tekever.challenge.showapi.repositories.UserClientRepository;

public class Utils {
    private final static String SECRET = "6B58703272357538782F413F4428472B4B6250655368566D5971337436763979";
    public Utils(){}
    
    public static String generateJwt(String email, String password){
        return Jwts.builder()
        .claim("email", email)
        .signWith(
            Keys.hmacShaKeyFor(Decoders.BASE64URL.decode(SECRET)))
        .compact();
    }

    public static HashMap<String, Object> validateJwt(String bearer){
       
        Claims _claims =  Jwts.parserBuilder()
        .setSigningKey(Keys.hmacShaKeyFor(Decoders.BASE64URL.decode(SECRET)))         
        .build()                    
        .parseClaimsJws(bearer).getBody();

        HashMap<String, Object> claims = new HashMap<>();
        claims.putAll(_claims);
        return claims;
    }

    public static UserClient getUserFromToken(String token, HttpServletRequest request, UserClientRepository userClientRepository){
        String userEmail="";
        if (token!=null){
          userEmail = (String) Utils.validateJwt(token).get("email");
        }
        else{
          Cookie[] _cookies = request.getCookies();
          if (_cookies!=null)
            for (Cookie c : _cookies){
                if (c.getName().equals("token")){
                userEmail = (String) Utils.validateJwt(c.getValue()).get("email");
                break;
                }
            }
    
        }
        if(userEmail.length()!=0 && userClientRepository.existsById(userEmail)){
          return userClientRepository.findById(userEmail).orElseThrow();
    
        }
        return null;
      }
      public static TvShow getByIdOrName(String id, TvShowsRepository showsRepository){
        try {
          int _id = Integer.parseInt(id);
          if (showsRepository.existsById(_id))
            return showsRepository.findById(_id).orElseThrow();
    
        } catch (NumberFormatException e) {
           
        }
        try{
          return showsRepository.findByName(id);
        }
        catch(Exception e){
    
        };
        return null;
        
      }
}
