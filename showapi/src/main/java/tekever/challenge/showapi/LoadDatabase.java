package tekever.challenge.showapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import tekever.challenge.showapi.repositories.GenreRepository;
import tekever.challenge.showapi.repositories.TvShowsRepository;
import tekever.challenge.showapi.worker.IProducer;
import tekever.challenge.showapi.worker.Monitor;
import tekever.challenge.showapi.worker.TConsumer;
import tekever.challenge.showapi.worker.TProducer;

@Configuration
class LoadDatabase {


  @Bean
  CommandLineRunner initDatabase(TvShowsRepository repository, GenreRepository genreRepository) {

    return args -> {
        Monitor monitor = new Monitor();
		TConsumer consumer = new TConsumer(monitor, repository, genreRepository);
		consumer.start();
		TProducer producer = new TProducer((IProducer)monitor);
		producer.start();
      
    };
  }
}