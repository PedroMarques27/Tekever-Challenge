package tekever.challenge.showapi.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import tekever.challenge.showapi.Utils;
import tekever.challenge.showapi.entities.UserClient;
import tekever.challenge.showapi.repositories.UserClientRepository;


import javax.persistence.EntityNotFoundException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
@Tag(name = "User Management", description = "Get/Add/Update/Delete Users, Login with credentials")
public class UserController {

  @Autowired
  private UserClientRepository userClientRepository;

  @GetMapping(value="/users", produces = "application/json")
  @Operation(summary = "Get all Users")
  public ResponseEntity<String> showAllUsers() {
    return ResponseEntity.ok().body((new Gson()).toJson(userClientRepository.findAll()));
  }

  @PostMapping("/users")
  @Operation(summary = "Add a new User")
  public ResponseEntity<String> signUp(@RequestBody UserClient _user) {
    if (!userClientRepository.existsById(_user.getEmail().strip().toLowerCase())){
      _user.hashPassword();
      _user.cleanId();
      userClientRepository.save(_user);
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.badRequest().body("User With Given Email Already Exists");
    
  }
  
  @DeleteMapping("/users")
  @Operation(summary = "Delete all Users")
  public ResponseEntity<Object> clearUsersDatabase() {
    userClientRepository.deleteAll();
    return ResponseEntity.noContent().build();
  }

  @GetMapping(value="/users/{id}", produces = "application/json")
  @Operation(summary = "Get User by Id")
  public ResponseEntity<String> getUserById(@PathVariable String id) {
    if (userClientRepository.existsById(id.strip().toLowerCase())){
      UserClient var1 = userClientRepository.findById(id.strip().toLowerCase()).orElseThrow(()->  new EntityNotFoundException());
      return ResponseEntity.ok().body((new Gson()).toJson(var1));
    }
    return ResponseEntity.badRequest().build();
  }
  @PutMapping("/users/{id}")
  @Operation(summary = "Update a specified User")
  public ResponseEntity<String> updateUser(@PathVariable String id, @RequestBody UserClient _user) {
    if (userClientRepository.existsById(id.strip().toLowerCase())){
      UserClient var1 = userClientRepository.findById(id.strip().toLowerCase()).orElseThrow(()->  new EntityNotFoundException());
      _user.hashPassword();
      _user.cleanId();
      _user.setFavorites(var1.getFavorites());
      if (!_user.getEmail().equalsIgnoreCase(id.strip()) && userClientRepository.existsById(_user.getEmail())){
        return ResponseEntity.badRequest().body("A User with that email already exists");
      }
      userClientRepository.deleteById(id.strip().toLowerCase());
      userClientRepository.save(_user);
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.badRequest().body("User was not updated");

  }

  @DeleteMapping("/users/{id}")
  @Operation(summary = "Get User by Id")
  public ResponseEntity<String> deleteUserById(@PathVariable String id) {
    if (userClientRepository.existsById(id.strip().toLowerCase())){
      userClientRepository.deleteById(id.strip().toLowerCase());
      return ResponseEntity.noContent().build();
    }
   
    return ResponseEntity.badRequest().body("User does not exist");
  }


  @GetMapping(value = "/login", produces = "application/json")
  public ResponseEntity<String> signIn(@RequestParam(name="email") String email, @RequestParam(name="password") String password, HttpServletResponse response) {
    if (userClientRepository.existsById(email.strip().toLowerCase())){
      UserClient var1 = userClientRepository.findById(email.strip().toLowerCase()).orElseThrow(() -> new EntityNotFoundException());
      if (var1.verifyPassword(password)){
        String token = Utils.generateJwt(email, password);
        response.addCookie(new Cookie("token",token));
        return ResponseEntity.ok().body(token);
      }
    }
    
    return new ResponseEntity<>(
      "Invalid Credentials", 
      HttpStatus.UNAUTHORIZED);
  }

 
 

}
