package tekever.challenge.showapi.controller;


import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import tekever.challenge.showapi.Utils;
import tekever.challenge.showapi.entities.Genre;
import tekever.challenge.showapi.entities.TvShow;
import tekever.challenge.showapi.entities.UserClient;
import tekever.challenge.showapi.repositories.TvShowsRepository;
import tekever.challenge.showapi.repositories.UserClientRepository;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;


@RestController
@Tag(name = "Shows", description = "Get Show Information, Get/Add/Remove Favorites")
@RequestMapping(path = "/shows")
public class ShowsController {
    

  @Autowired
  private TvShowsRepository showsRepository;

  @Autowired
  private UserClientRepository userClientRepository;

  
  @GetMapping(value="", produces = "application/json")
  @Operation(summary = "Get all Shows Order by any field available, sort (ASC/DESC)")
  public ResponseEntity<String> showAllShows(@RequestParam(required=false,defaultValue = "id",name="orderBy") String orderBy,@RequestParam(required=false,defaultValue = "0",name="page") int page, @RequestParam(required=false, name="genre") String genre) {
    
    if (genre!=null){
      return ResponseEntity.ok().body((new Gson()).toJson(showsRepository.findByGenres(new Genre(genre), Sort.by(orderBy)))) ;
    }
    return ResponseEntity.ok().body((new Gson()).toJson(showsRepository.findAll(PageRequest.of(page, 10, Sort.by(orderBy)))));
  }
  @DeleteMapping("")
  @Operation(summary = "Clear Shows Database")
  public ResponseEntity<String> clearShowsDatabase() {
    for (UserClient u: userClientRepository.findAll()){
        u.clearFavorites();
        userClientRepository.save(u);
    }
    showsRepository.deleteAll();
    return ResponseEntity.noContent().build();
  }
  @GetMapping(value="/{id}", produces = "application/json")
  @Operation(summary = "Get Show By Id")
  public ResponseEntity<String> showById(@PathVariable String id) {
    TvShow got = Utils.getByIdOrName(id, showsRepository);
    if (got != null && showsRepository.existsById(got.getId())){
        return ResponseEntity.ok().body((new Gson()).toJson(got));
      }
    return ResponseEntity.badRequest().body("Show does not Exist");
  }
  @DeleteMapping("/{id}")
  @Operation(summary = "Remove Show By Id")
  public ResponseEntity<String> removeById(@PathVariable String id) {
    TvShow got = Utils.getByIdOrName(id, showsRepository);
    for (UserClient u: userClientRepository.findAll()){
        u.removeFavorite(got);
        userClientRepository.save(u);
    }
    if (got!=null){
        showsRepository.deleteById(got.getId());
        return ResponseEntity.noContent().build();
      }
    return ResponseEntity.badRequest().body("Show with given id or name does not exist");
  }

  @GetMapping(value="/{id}/episodes", produces = "application/json")
  @Operation(summary = "Get Show Episodes")
  public ResponseEntity<String> showEpisodesByShowId(@PathVariable String id) {
    TvShow got = Utils.getByIdOrName(id, showsRepository);
    if (got != null && showsRepository.existsById(got.getId())){
        return ResponseEntity.ok().body((new Gson()).toJson(got.getEpisodes()));
      }
    return ResponseEntity.badRequest().body("Show with given id or name does not exist");
  }

  @GetMapping(value="/{id}/details", produces = "application/json")
  @Operation(summary = "Get Show Details By Id")
  public ResponseEntity<String> showDetailsById(@PathVariable String id) {
    RequestSpecification request = RestAssured.given();
    Response response = request.get(String.format("https://www.episodate.com/api/show-details?q=%s", id));
    JSONObject show = new JSONObject();
    try{
        show = (new JSONObject(response.body().asString())).getJSONObject("tvShow");
    }catch (JSONException e){
        
    }
    return ResponseEntity.ok().body((new Gson()).toJson(show.toMap()));
   
  }




  @GetMapping(value="/{id}/favorites")
  @Operation(summary = "Add Show to Favorites (Requires Auth Token)")
  public ResponseEntity<String> favorite(@PathVariable String id, @RequestParam(required = false, name = "token") String token, HttpServletRequest request) {
      UserClient var1 = Utils.getUserFromToken(token, request, userClientRepository);
      TvShow show = Utils.getByIdOrName(id, showsRepository);
      if (var1 == null){
        return new ResponseEntity<>(
            "Invalid Credentials", 
            HttpStatus.UNAUTHORIZED);
      }else if (show!=null ){
        Set<TvShow> favs = var1.getFavorites();
        favs.add(show);
        var1.setFavorites(favs);
        userClientRepository.save(var1);
        
        return ResponseEntity.noContent().build();
      }
      return ResponseEntity.badRequest().body("TvShow does not exists");
    
    }
  

  @DeleteMapping("/{id}/favorites")
  @Operation(summary = "Remove Show from Favorites (Requires Auth Token)")
  public ResponseEntity<String> unfavorite(@PathVariable String id, @RequestParam(required = false, name = "token") String token, HttpServletRequest request) {
      UserClient var1 = Utils.getUserFromToken(token, request, userClientRepository);
      TvShow show = Utils.getByIdOrName(id, showsRepository);
      if (var1 == null){
        return new ResponseEntity<>(
        "Invalid Credentials", 
        HttpStatus.UNAUTHORIZED);
      }
      else if (show!=null){
        var1.removeFavorite(show);
        userClientRepository.save(var1);
        return ResponseEntity.noContent().build();
      }
      return ResponseEntity.badRequest().body("TvShow does not exists");
      
    }
    
  @GetMapping("/favorites")
  @Operation(summary = "Get Favorites (Requires Auth Token)")
  public ResponseEntity<String> getFavorites(@RequestParam(required = false, name = "token") String token, HttpServletRequest request) {
    UserClient var1 = Utils.getUserFromToken(token, request, userClientRepository);
    if (var1!=null){
      return ResponseEntity.ok().body((new Gson()).toJson(var1.getFavorites()));
    }
    return new ResponseEntity<>(
        "Invalid Credentials", 
        HttpStatus.UNAUTHORIZED);
  }

  

}
