package tekever.challenge.showapi.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tekever.challenge.showapi.entities.UserClient;
import tekever.challenge.showapi.repositories.UserClientRepository;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import com.fasterxml.jackson.databind.ObjectMapper;


@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    private UserClientRepository db;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    
    UserClient RECORD_1 = new UserClient("A1", "U1", "PASS1");
    UserClient RECORD_2 = new UserClient("B1", "U2", "PASS2");
    UserClient RECORD_3 = new UserClient("C1", "U3", "PASS3");
  
    @Test
    void testGetUsers() throws Exception {
        List<UserClient> records = new ArrayList<>(Arrays.asList(RECORD_1, RECORD_2, RECORD_3));
        Mockito.when(db.findAll()).thenReturn(records);
        
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.size()").value(records.size()));;
    }
    @Test
    void testGetUserById() throws Exception {
        Mockito.when(db.findById("a1")).thenReturn(Optional.of(RECORD_1));
        Mockito.when(db.existsById("a1")).thenReturn(true);
        
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users/a1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$.email").value("A1"));;
    }

    @Test
    void testPostNewUser() throws Exception {
        Mockito.when(db.existsById("a1")).thenReturn(false);
        
        mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .content(objectMapper.writeValueAsString(RECORD_1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));
      
    }
  

   
    @Test
    void testPostExistingUser() throws Exception {

        Mockito.when(db.existsById("a1")).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .content(objectMapper.writeValueAsString(RECORD_1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }
  

    @Test
    void testClearDatabase() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/users"))
                .andExpect(status().is(204));
    }
    
    @Test
    void testUpdateUserById() throws Exception {
        UserClient RECORD_1_ALT = new UserClient("A4", "U5", "PASS1");

        Mockito.when(db.existsById("a1")).thenReturn(true);
        Mockito.when(db.findById("a1")).thenReturn(Optional.of(RECORD_1));
        Mockito.when(db.existsById("a4")).thenReturn(false);

        mockMvc.perform(MockMvcRequestBuilders
                .put("/users/a1")
                .content(objectMapper.writeValueAsString(RECORD_1_ALT))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(204));
    }
  

    @Test
    void testUpdateNonExistingUserById() throws Exception {
        UserClient RECORD_1_ALT = new UserClient("A4", "U5", "PASS1");

        Mockito.when(db.existsById("a1")).thenReturn(false);

        mockMvc.perform(MockMvcRequestBuilders
                .put("/users/A1")
                .content(objectMapper.writeValueAsString(RECORD_1_ALT))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }
    
    @Test
    void testDeleteUserById() throws Exception {
        Mockito.when(db.existsById("a1")).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/a1"))
                .andExpect(status().is(204));
    }
    @Test
    void testDeleteUnavailableUserById() throws Exception {
        Mockito.when(db.existsById("a1")).thenReturn(false);

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/a1"))
                .andExpect(status().is(400));
    }
   
}
